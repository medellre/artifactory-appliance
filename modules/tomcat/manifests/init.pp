#-----------------------------------------------------------------------------
#   Copyright (c) 2012 Rene Medellin
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#-----------------------------------------------------------------------------

class tomcat {
  $packages = $::operatingsystem ? {
    CentOS  => [ 'tomcat6', 'tomcat6-webapps', 'tomcat6-admin-webapps' ],
    default => [ 'tomcat6', 'tomcat6-webapps', 'tomcat6-admin-webapps' ],
  }

  $service = $::operatingsystem ? {
    CentOS  => 'tomcat6',
    default => 'tomcat6',
  }

  package { $packages:
    ensure => latest,
  }  

  user { 'tomcat':
  	groups => 'vagrant', 
  	require => Package[$service],
  }
  
  service { $service:
  	ensure => running,
  	enable => true,
  	require => User['tomcat'],
  }

  file { '/etc/tomcat6/tomcat6.conf':
  	source => "puppet:///modules/tomcat/tomcat.conf",
  	require => Service[$service],
  }

}
