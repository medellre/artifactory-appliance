/*
 * Copyright (C) 2012 Rene Medellin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.artifactory.exception.CancelException 
import org.artifactory.repo.RepoPathFactory 

download { 
  altResponse { request, responseRepoPath -> 
    if ( security.isAnonymous() ) { 
      status = 403 
      message = "Artifact download by Anonymous user not permitted per artifact policy" 
    } else if ( "${security.currentUsername()}" != "admin" ) { 
        // TODO: Enable group lookups 
        if (  responseRepoPath.getRepoKey() =~ /.*\-release/ ) { 
          status = 403 
          message = "Artifact download from ${responseRepoPath.getRepoKey()} not permitted per artifact policy" 
        } 
    } 
  } 
} 

storage { 

  beforeCreate { item -> 
    def item_repo_base = item.repoKey.split(/-/)[0] 
    if ( "${item.repoKey}" =~ /.*\-dev/ ) { 
      log.warn "Artifact deploy of ${item} for ${item.repoKey} by ${security.currentUsername()}" 
    } else if ( "${item.repoKey}" =~ /.*\-test/ ) { 
        def preExisting = RepoPathFactory.create(item_repo_base+"-dev", "${item.relPath}") 
        if ( "${security.currentUsername()}" == "admin" ) { 
          log.warn "Artifact deploy of ${item} for ${item.repoKey} by ${security.currentUsername()}" 
        } else if ( repositories.exists(preExisting) ) { 
            log.warn "Artifact deploy of ${item} for ${item.repoKey} by ${security.currentUsername()}" 
        } else { 
            throw new CancelException("Artifact deploy not permitted for ${item.repoKey} by artifact policy", 403) 
       } 
    } else if ( "${item.repoKey}" =! /.*\-release/ ) { 
        def preExisting = RepoPathFactory.create(item_repo_base+"-test", "${item.relPath}") 
        if ( "${security.currentUsername()}" == "admin" ) { 
          log.warn "Artifact deploy of ${item} for ${item.repoKey} by ${security.currentUsername()}" 
        } else if ( repositories.exists(preExisting) ) { 
            log.warn "Artifact deploy of ${item} for ${item.repoKey} by ${security.currentUsername()}" 
        } else { 
            throw new CancelException("Artifact deploy not permitted for ${item.repoKey} by artifact policy", 403) 
       } 
    } else { 
      throw new CancelException("Artifact deploy not permitted for ${item.repoKey} by artifact policy", 403) 
    } 
  } 

} 
