#-----------------------------------------------------------------------------
#   Copyright (c) 2012 Rene Medellin
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#-----------------------------------------------------------------------------

class artifactory {
  # This is the main module for Artifactory
  require motd
  require iptables
  require downloads
  require unzip
  require java
  require tomcat

  file { '/opt/artifactory':
  	ensure => directory,
  	owner => 'tomcat',
  }

  file { '/opt/downloads/artifactory':
  	ensure => 'directory',
  	mode => '0777',
  }

  downloads::wget { "artifactory-2.6.4.zip":
  	url => 'http://sourceforge.net/projects/artifactory/files/artifactory/2.6.4/artifactory-2.6.4.zip/download',
  	target_dir => '/opt/downloads/artifactory',
  	creates => "/opt/downloads/artifactory/artifactory-2.6.4.zip",
  }

  downloads::unzip { "/opt/downloads/artifactory/artifactory-2.6.4":
  	target_dir => '/opt/downloads/artifactory',
  	creates => '/opt/downloads/artifactory/artifactory-2.6.4',
  	subscribe => Downloads::Wget['artifactory-2.6.4.zip'],
  }

  exec { "deploy-artifactory-war":
		command => "/bin/cp /opt/downloads/artifactory/artifactory-2.6.4/webapps/artifactory.war /usr/share/tomcat6/webapps/.",
		creates => "/usr/share/tomcat6/webapps/artifactory.war",
		user => "root",
		require => Downloads::Unzip["/opt/downloads/artifactory/artifactory-2.6.4"],
  }
  
  file { '/usr/share/tomcat6/webapps/artifactory.war':
  	ensure => file,
	require => Exec['deploy-artifactory-war'],
  }

  file { '/opt/artifactory/etc/artifactory.system.properties':
    ensure => file,
    source => 'puppet:///modules/artifactory/artifactory.system.properties',
  }

}
